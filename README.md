# Futify

Native qml spotify client

# Install

[![OpenStore](https://open-store.io/badges/en_US.svg)](https://open-store.io/app/futify.frenchutouch)

# Develop

### Ubuntu dependencies
Dependencies of nannu-c/qml-go:
```
apt-get install qtdeclarative5-dev qtbase5-private-dev qtdeclarative5-private-dev libqt5opengl5-dev
ln -s /usr/include/x86_64-linux-gnu/qt5/QtCore/<QT_VERSION>/QtCore /usr/include/
```

### Fedora dependencies
```
sudo dnf install qt5-qtdeclarative-devel  qt5-qtbase-private-devel  qt5-qtbase-gui
```

### Initialisation of project
```
$ cd \$GOPATH
$ git clone 
$ go get .
```

### Run application on desktop
```
clickable desktop --dark
```
### Run application on device 
```
rm -rf build && clickable clean && clickable --arch arm64 || clickable logs --arch arm64
```





## License

Copyright (C) 2022 Romain Maneschi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
