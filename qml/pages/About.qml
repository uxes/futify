import QtQuick 2.12
import Lomiri.Components 1.3

Page {
    id: page

    header: PageHeader {
        id: header
        title: qsTr("About")
        StyleHints {
            dividerColor: LomiriColors.green
        }
    }

    Flickable {
        anchors.top: header.bottom
        width: parent.width
        height: parent.height - header.height
        contentWidth: parent.width
        contentHeight: units.gu(160)
        Column {
            spacing: units.gu(4)
            padding: units.gu(2)

            Column {
                spacing: units.gu(2)
                padding: units.gu(1)
                Label { 
                    text: qsTr('App:')
                    textSize: Label.Large
                }
                Column {
                    spacing: units.gu(2)
                    padding: units.gu(1)
                    Label {
                        text: qsTr('Futify is an unofficial native spotify client.')
                    }
                    Button {
                        iconName: 'stock_website'
                        text: 'gitlab.com/frenchutouch/futify'
                        width: units.gu(40)
                        onClicked: {
                            Qt.openUrlExternally('https://gitlab.com/frenchutouch/futify')
                        }
                    }
                    Button {
                        iconName: 'preferences-desktop-login-items-symbolic'
                        text: qsTr('issues')
                        width: units.gu(40)
                        onClicked: {
                            Qt.openUrlExternally('https://gitlab.com/frenchutouch/futify/-/issues')
                        }
                    }
                }
            }

            Column {
                spacing: units.gu(2)
                padding: units.gu(1)
                Label { 
                    text: qsTr('Author:')
                    textSize: Label.Large
                }
                Column {
                    spacing: units.gu(2)
                    padding: units.gu(1)
                    Label {
                        text: 'Romain Maneschi'
                    }
                    Button {
                        iconName: 'stock_website'
                        text: 'romain.maneschi.fr'
                        width: units.gu(40)
                        onClicked: {
                            Qt.openUrlExternally('http://romain.maneschi.fr')
                        }
                    }
                    Button {
                        iconName: 'twitter-symbolic'
                        text: '@RmManeschi'
                        width: units.gu(40)
                        onClicked: {
                            Qt.openUrlExternally('https://twitter.com/RmManeschi/')
                        }
                    }
                }
            }

            Column {
                spacing: units.gu(2)
                padding: units.gu(1)

                Label { 
                    text: qsTr('Contributors:')
                    textSize: Label.Large
                }

                ListView {
                    height: page.height - header.height
                    width: page.width
                    clip: true
                    model: spotSession.nbContributors()
                    delegate: ListItem {
                        height: units.gu(10)
                        width: parent.width

                        property var contributor: spotSession.getContributor(index)

                        Row {
                            height: parent.height
                            width: parent.width
                            padding: units.gu(2)
                            spacing: units.gu(2)

                            Item {
                                id: itemImage
                                width: units.gu(6)
                                height: units.gu(6)
                                anchors.verticalCenter: parent.verticalCenter
                                Image {
                                    id: image
                                    source: contributor.avatarUrl
                                    width: parent.width
                                    height: parent.height
                                    anchors.verticalCenter: parent.verticalCenter
                                    fillMode: Image.PreserveAspectFit
                                }
                            }
                            Column {
                                anchors.verticalCenter: itemImage.verticalCenter
                                width: parent.width - itemImage.width - parent.spacing - parent.padding
                                Label { text: contributor.name ; width: parent.width; elide: Text.ElideLeft; wrapMode: Text.Wrap; maximumLineCount: 1; textSize: Label.Large }
                            }
                        }
                        onClicked: {
                            Qt.openUrlExternally(contributor.link)
                        }
                    }
                }
            }
        }
    }

}