import QtQuick 2.12
import Lomiri.Components 1.3

import "../components"
import "../model"

Page {
    id: page

    property PlaylistsType playlists

    signal startPlaylist(var playlist)
    signal selectPlaylist(var playlist)

    function refresh(playlistsTypeFactory) {
        if (playlists.uuid != '') {
            playlists.size = 0;
            playlists = playlistsTypeFactory.createObject(page, spotSession.getPlaylists(playlists.uuid));
        }
    }

    header: PageHeader {
        id: header
        title: playlists.getName() + " (" + playlists.totalSize + ")"
        StyleHints {
            dividerColor: LomiriColors.green
        }
    }

    PlaylistsView {
        id: playlistView
        playlists: page.playlists
        getIndex: function(playlist, index) {
            return spotSession.getPlaylistByIndex(playlists.uuid, index);
        }
        size: page.playlists.size
        anchors.top: header.bottom
        height: page.height - header.height
        width: page.width
        onSelect: function(playlist) {
            page.selectPlaylist(playlist)
        }
        onStart: function(playlist) {
            page.startPlaylist(playlist)
        }
    }
}