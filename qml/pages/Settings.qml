import QtQuick 2.12
import QtQuick.Layouts 1.3
import Lomiri.Components 1.3
import Lomiri.Components.Pickers 1.0

Page {
    id: settings
    anchors.fill: parent

    property var _needSave: false;

    signal logout()

    Component.onCompleted: {
        console.log("OnComplete", settings.width);
    }

    function init() {
        qualityColumnChoises.currentIndex = spotSession.settings.quality;
        themeColumnChoises.currentIndex = spotSession.settings.theme;
        langColumnChoises.currentIndex = spotSession.settings.lang;
        errorSongColumnChoises.currentIndex = spotSession.settings.errorSong;
    }

    header: PageHeader {
        id: header
        title: qsTr("Settings")
        StyleHints {
            dividerColor: LomiriColors.green
        }
        trailingActionBar.actions: [
            Action {
                visible: _needSave
                text: qsTr("Reset")
                iconName: "reset"
                onTriggered: {
                    settings.init();
                    _needSave = false;
                }
            },
            Action {
                visible: _needSave
                text: qsTr("Save")
                iconName: "ok"
                onTriggered: {
                    spotSession.saveSettings(qualityColumnChoises.currentIndex, themeColumnChoises.currentIndex, langColumnChoises.currentIndex, errorSongColumnChoises.currentIndex);

                    if (spotSession.settings.theme === 0) {
                        theme.name = "Lomiri.Components.Themes.SuruDark"
                    } else if(spotSession.settings.theme === 1) {
                        theme.name = "Lomiri.Components.Themes.Ambiance"
                    } else {
                        theme.name = ""
                    }

                    _needSave = false;
                    settings.init();// force reload config in case of Retranslate which reload view
                }
            }
        ]
    }

    Flickable {
        anchors.top: header.bottom
        width: parent.width
        height: parent.height - header.height
        contentWidth: parent.width
        contentHeight: units.gu(90)

        Column {
            id: columnSettings
            spacing: units.gu(4)
            padding: units.gu(2)
            width: parent.width

            Column {
                id: themeColumn
                spacing: units.gu(2)
                width: parent.width
                Row {
                    spacing: units.gu(1)
                    Icon {
                        height: themeLabel.height
                        name: 'audio-x-generic-symbolic'
                    }
                    Label {
                        id: themeLabel
                        text: qsTr('Theme:')
                        textSize: Label.Large
                    }
                }
                ListView {
                    id: themeColumnChoises
                    height: units.gu(6)
                    width: parent.width - units.gu(3)
                    orientation: ListView.Horizontal
                    model: [qsTr('Dark'), qsTr('Light'), qsTr('System')]
                    delegate: ListItem {
                        width: units.gu(16)
                        height: parent.height
                        divider.visible: false
                        Label {
                            text: modelData
                            anchors.verticalCenter: parent.verticalCenter
                            width: parent.width
                            elide: Text.ElideLeft
                            wrapMode: Text.Wrap
                            maximumLineCount: 3
                            horizontalAlignment: Text.AlignHCenter
                        }
                        onClicked: {
                            themeColumnChoises.currentIndex = index;
                            _needSave = true;
                        }
                    }
                    currentIndex: 1
                    focus: true
                    highlight:  Rectangle {
                        width: themeColumnChoises.width
                        height: units.gu(16)
                        color: LomiriColors.green
                        y: themeColumnChoises.currentItem == null ? -1 : themeColumnChoises.currentItem.y
                        Behavior on x {
                            SpringAnimation {
                                spring: 3
                                damping: 0.2
                            }
                        }
                    }
                }
            }

            Column {
                id: langColumn
                spacing: units.gu(2)
                width: parent.width
                Row {
                    spacing: units.gu(1)
                    Icon {
                        height: langLabel.height
                        name: 'audio-x-generic-symbolic'
                    }
                    Label {
                        id: langLabel
                        text: qsTr('Lang:')
                        textSize: Label.Large
                    }
                }
                ListView {
                    id: langColumnChoises
                    height: units.gu(6)
                    width: parent.width - units.gu(3)
                    orientation: ListView.Horizontal
                    model: [qsTr('System'), qsTr('German'), qsTr('English'), qsTr('Spanish'), qsTr('French'), qsTr('Hungarian'), qsTr('Dutch'), qsTr('Swedish'), qsTr('Czech')]
                    delegate: ListItem {
                        width: units.gu(16)
                        height: parent.height
                        divider.visible: false
                        Label {
                            text: modelData
                            anchors.verticalCenter: parent.verticalCenter
                            width: parent.width
                            elide: Text.ElideLeft
                            wrapMode: Text.Wrap
                            maximumLineCount: 3
                            horizontalAlignment: Text.AlignHCenter
                        }
                        onClicked: {
                            langColumnChoises.currentIndex = index;
                            _needSave = true;
                        }
                    }
                    currentIndex: 1
                    focus: true
                    highlight:  Rectangle {
                        width: langColumnChoises.width
                        height: units.gu(16)
                        color: LomiriColors.green
                        y: langColumnChoises.currentItem == null ? -1 : langColumnChoises.currentItem.y
                        Behavior on x {
                            SpringAnimation {
                                spring: 3
                                damping: 0.2
                            }
                        }
                    }
                }
            }

            Column {
                id: qualityColumn
                spacing: units.gu(2)
                width: parent.width
                Row {
                    spacing: units.gu(1)
                    Icon {
                        height: qualityLabel.height
                        name: 'audio-x-generic-symbolic'
                    }
                    Label {
                        id: qualityLabel
                        text: qsTr('Quality:')
                        textSize: Label.Large
                    }
                }
                ListView {
                    id: qualityColumnChoises
                    height: units.gu(6)
                    width: parent.width - units.gu(3)
                    orientation: ListView.Horizontal
                    model: [qsTr('low: prefer data over quality'), qsTr('medium'), qsTr('high: prefer quality over data')]
                    delegate: ListItem {
                        width: units.gu(16)
                        height: parent.height
                        divider.visible: false
                        Label {
                            text: modelData
                            anchors.verticalCenter: parent.verticalCenter
                            width: parent.width
                            elide: Text.ElideLeft
                            wrapMode: Text.Wrap
                            maximumLineCount: 3
                            horizontalAlignment: Text.AlignHCenter
                        }
                        onClicked: {
                            qualityColumnChoises.currentIndex = index;
                            _needSave = true;
                        }
                    }
                    currentIndex: 1
                    focus: true
                    highlight:  Rectangle {
                        width: qualityColumnChoises.width
                        height: units.gu(16)
                        color: LomiriColors.green
                        y: qualityColumnChoises.currentItem == null ? -1 : qualityColumnChoises.currentItem.y
                        Behavior on x {
                            SpringAnimation {
                                spring: 3
                                damping: 0.2
                            }
                        }
                    }
                }
            }

            Column {
                id: errorSongColumn
                spacing: units.gu(2)
                width: parent.width
                Row {
                    spacing: units.gu(1)
                    Icon {
                        height: errorSong.height
                        name: 'audio-x-generic-symbolic'
                    }
                    Label {
                        id: errorSong
                        text: qsTr('Error song:')
                        textSize: Label.Large
                    }
                }
                ListView {
                    id: errorSongColumnChoises
                    height: units.gu(6)
                    width: parent.width - units.gu(3)
                    orientation: ListView.Horizontal
                    model: [qsTr('silence: 3 seconds'), qsTr('error: usefull to know what happens')]
                    delegate: ListItem {
                        width: units.gu(16)
                        height: parent.height
                        divider.visible: false
                        Label {
                            text: modelData
                            anchors.verticalCenter: parent.verticalCenter
                            width: parent.width
                            elide: Text.ElideLeft
                            wrapMode: Text.Wrap
                            maximumLineCount: 3
                            horizontalAlignment: Text.AlignHCenter
                        }
                        onClicked: {
                            errorSongColumnChoises.currentIndex = index;
                            _needSave = true;
                        }
                    }
                    currentIndex: 1
                    focus: true
                    highlight:  Rectangle {
                        width: errorSongColumnChoises.width
                        height: units.gu(16)
                        color: LomiriColors.green
                        y: errorSongColumnChoises.currentItem == null ? -1 : errorSongColumnChoises.currentItem.y
                        Behavior on x {
                            SpringAnimation {
                                spring: 3
                                damping: 0.2
                            }
                        }
                    }
                }
            }

            Column {
                spacing: units.gu(2)
                width: parent.width - units.gu(3)
                Row {
                    spacing: units.gu(1)
                    Icon {
                        height: accoutnLabel.height
                        name: 'preferences-desktop-accounts-symbolic'
                    }
                    Label {
                        id: accoutnLabel
                        text: qsTr('Account:')
                        textSize: Label.Large
                    }
                }
                RowLayout {
                    width: parent.width - units.gu(3)

                    Item {
                        Layout.fillWidth: true
                    }
                    Button {
                        text: qsTr('Log out')
                        onClicked: logout()
                    }
                }
            }
        }
    }
}