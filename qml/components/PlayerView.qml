import QtQuick 2.12
import QtQuick.Layouts 1.11
import Lomiri.Components 1.3
import QtMultimedia 5.12
import QtGraphicalEffects 1.0

RowLayout {
    
    property var player
    signal toggleQueue()
    signal toggleNowPlaying()
    signal playbackModeRandom() // todo: presunout do NowPlaying
    signal playbackModeLoop() // todo: presunout do NowPlaying
    signal playbackModeRepeatOne() // todo: presunout do NowPlaying

    spacing: units.gu(2)
    Layout.topMargin: units.gu(2)

    Item {
        id: playPauseImage
        width: parent.height
        height: parent.height
        anchors.verticalCenter: parent.verticalCenter
        visible: player.playlist.itemCount > 0

        Image {
            id: image
            source: player.playlist.itemCount <= 0 || player.playlist.currentIndex < 0 ? "" : spotSession.getTrackByAlbum('queue', player.playlist.currentIndex).image
            width: parent.width
            height: parent.height
            anchors.verticalCenter: parent.verticalCenter
            fillMode: Image.PreserveAspectFit
            visible: false
        }
        OpacityMask {
            anchors.fill: image
            source: image
            width: image.width
            height: image.height
            maskSource: Rectangle {
                width: image.width
                height: image.height
                radius: 5
                visible: false // this also needs to be invisible or it will cover up the image
            }
        }
        Rectangle {
            color: "#99FFFFFF"
            width: units.gu(3)
            height: units.gu(3)
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            radius: 50
        }
        Icon {
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            name: player.bufferProgress < 1.0 ? 'sync-updating' : player.playbackState == Audio.PlayingState ? "media-playback-pause" : "media-playback-start"
            color: "#000000"
            width: units.gu(3)
            height: units.gu(3)
        }
        TapHandler {
            onTapped: {
            toggleNowPlaying()
            /* todo: presunout do NowPlaying
                if(player.bufferProgress < 1.0) {
                    return;
                }
                if (player.playbackState != Audio.PlayingState) {
                    player.play();
                } else {
                    player.pause();
                }*/
            }
        }
    }

    Column {
        Layout.fillWidth: true
        height: parent.height
        RowLayout {
            width: parent.width
            Label {
                text: player.playlist.itemCount == 0 || player.playlist.currentIndex < 0 ? qsTr("No song") : player.bufferProgress < 1.0 ? qsTr("Buffering"): spotSession.getTrackByAlbum('queue', player.playlist.currentIndex).name
                elide: Text.ElideLeft
                wrapMode: Text.Wrap
                maximumLineCount: 1
            }
            Item {
                Layout.fillWidth: true
            }
            Label {
                text: player.position < 0 ? "0:00" : Math.floor(player.position / 60000) + ":" + (((player.position % 60000) / 1000).toFixed(0) < 10 ? '0' : '') + ((player.position % 60000) / 1000).toFixed(0)
                maximumLineCount: 1
            }
        }
        Slider {
            id: slider
            function formatValue(millis) { 
                var minutes = Math.floor(millis / 60000);
                var seconds = ((millis % 60000) / 1000).toFixed(0);
                return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
            }
            width: parent.width
            value: player.position
            live: false
            minimumValue: 0.0
            maximumValue: player.duration
            onTouched: function() {
                player.seek(slider.value)
            }
        }
    }

    Column {
        Layout.alignment: Qt.AlignTop
        topPadding: units.gu(1)
        width: units.gu(6)
        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            text: Math.min((player.playlist.currentIndex + 1), player.playlist.itemCount) + " / " + player.playlist.itemCount
            textSize: Label.Small
        }
        Row {
            spacing: units.gu(1)
            Icon {
                name: 'media-seek-backward'
                color: player.playlist.currentIndex > 0 ? "#ffffff" : "#333333"
                width: units.gu(3)

                TapHandler {
                    onTapped: {
                        if (player.playlist.currentIndex > 0) {
                            player.playlist.previous()
                        }
                    }
                }
            }
            Icon {
                name: 'media-seek-forward'
                color: player.playlist.currentIndex < player.playlist.itemCount - 1 ? "#ffffff" : "#333333"
                width: units.gu(3)

                TapHandler {
                    onTapped: {
                        if (player.playlist.currentIndex < player.playlist.itemCount - 1) {
                            player.playlist.next()
                        }
                    }
                }
            }
        }
    }

    Column {
            Layout.alignment: Qt.AlignTop
            topPadding: units.gu(1.5)
            rightPadding: units.gu(2)
            Icon {
                name: "view-list-symbolic"
                color: player.playlist.playbackMode == Playlist.Random ? LomiriColors.green : theme.palette.normal.backgroundSecondaryText
                width: units.gu(2)

                TapHandler {
                    onTapped: {
                        toggleQueue()
                    }
                }
            }
        }
/* todo: presunout do NowPlaying
    Column {
        Layout.alignment: Qt.AlignTop
        topPadding: units.gu(1.5)
        rightPadding: units.gu(2)
        Icon {
            name: "media-playlist-shuffle"
            color: player.playlist.playbackMode == Playlist.Random ? LomiriColors.green : theme.palette.normal.backgroundSecondaryText
            width: units.gu(2)

            TapHandler {
                onTapped: {
                    if (player.playlist.playbackMode == Playlist.Random) {
                        player.playlist.playbackMode = Playlist.Sequential;
                    } else {
                        player.playlist.playbackMode = Playlist.Random;
                        playbackModeRandom()
                    }
                }
            }
        }
        Icon {
            name: player.playlist.playbackMode == Playlist.CurrentItemInLoop ? "media-playlist-repeat-one" : "media-playlist-repeat"
            color: player.playlist.playbackMode == Playlist.Loop || player.playlist.playbackMode == Playlist.CurrentItemInLoop ? LomiriColors.green : theme.palette.normal.backgroundSecondaryText
            width: units.gu(2)

            TapHandler {
                onTapped: {
                    if (player.playlist.playbackMode == Playlist.Sequential) {
                        player.playlist.playbackMode = Playlist.CurrentItemInLoop;
                        playbackModeRepeatOne();
                    } else if (player.playlist.playbackMode == Playlist.CurrentItemInLoop) {
                        player.playlist.playbackMode = Playlist.Loop;
                        playbackModeLoop();
                    } else {
                        player.playlist.playbackMode = Playlist.Sequential;
                    }
                }
            }
        }
    }
    */
}
