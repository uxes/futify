package server

import (
	"context"
	"fmt"

	"github.com/zmb3/spotify/v2"
)

const TopTracksPlaylistUUID = "TopTracks"
const UserTracksUUID = "UserTracks"

func (s *Session) LoadPlaylist(playlistUuid string) string {
	if s.GetPlaylist(playlistUuid) != nil && s.GetPlaylist(playlistUuid).TotalSize == len(s.GetPlaylist(playlistUuid).tracks) {
		// already loaded don't re-download it
		fmt.Println("Found uuid return " + playlistUuid)
		return playlistUuid
	}
	if playlistUuid == TopTracksPlaylistUUID {
		return s.loadTopTracks(false)
	}
	if playlistUuid == UserTracksUUID {
		return s.loadUserTracks(false)
	}
	if s.IsLogged || s.TryLogin() {
		fmt.Println("Preventive re-logging")
	} else {
		fmt.Println("Could not relogin")
	}
	a, err := s.spotAPIClient.GetPlaylist(context.Background(), spotify.ID(playlistUuid))
	if err != nil {
		fmt.Println("Login token may have expired")
		fmt.Println(err)
	}
	myTracks := make([]*Track, 0)
	for ; err == nil; err = s.spotAPIClient.NextPage(context.Background(), &a.Tracks) {
		for _, t := range a.Tracks.Tracks {
			myTracks = append(myTracks, NewTrack(&t.Track, s))
		}
	}
	isFollow := playlistFollowFalse
	for _, p := range s.playlists[s.LoadUserPlaylists()].playlists {
		if p.Uuid == playlistUuid {
			isFollow = playlistFollowTrue
		}
	}
	s.playlist[playlistUuid] = NewPlaylist(playlistUuid, a.Name, a.Owner.DisplayName, SelectImageURL(a.Images), a.Followers.Count, uint(a.Tracks.Total), myTracks, playlistKindPlaylist, isFollow)
	fmt.Println(fmt.Sprintf("Found %d songs added in %s", len(myTracks), playlistUuid))
	return playlistUuid
}

func (s *Session) FollowPlaylist(playlistId string, kind string) {
	if kind == playlistKindPlaylist {
		err := s.spotAPIClient.FollowPlaylist(context.Background(), spotify.ID(playlistId), false)
		if err != nil {
			fmt.Println(err)
			return
		}
		s.playlists[s.LoadUserPlaylists()] = nil
		p := s.playlist[playlistId]
		s.playlist[playlistId] = NewPlaylist(playlistId, p.Name, p.Owner, p.Image, uint(p.Followers), uint(p.TotalSize), p.tracks, p.Kind, playlistFollowTrue)
		s.LoadUserPlaylists()
	} else if kind == playlistKindAlbum {
		err := s.spotAPIClient.AddAlbumsToLibrary(context.Background(), spotify.ID(playlistId))
		if err != nil {
			fmt.Println(err)
			return
		}
		s.playlists[s.LoadUserAlbums()] = nil
		p := s.albums[playlistId]
		s.albums[playlistId] = NewAlbum(playlistId, p.Name, p.Group, []spotify.SimpleArtist{}, p.Image, p.tracks, playlistFollowTrue)
		s.LoadUserAlbums()
	} else if kind == playlistKindShow {
		err := s.spotAPIClient.SaveShowsForCurrentUser(context.Background(), []spotify.ID{spotify.ID(playlistId)})
		if err != nil {
			fmt.Println(err)
			return
		}
		s.playlists[s.LoadUserShows()] = nil
		p := s.show[playlistId]
		s.show[playlistId] = NewShow(playlistId, p.Name, p.Owner, p.Image, uint(p.Followers), p.tracks, playlistFollowTrue)
		s.LoadUserShows()
	}
}

func (s *Session) UnfollowPlaylist(playlistId string, kind string) {
	if kind == playlistKindPlaylist {
		err := s.spotAPIClient.UnfollowPlaylist(context.Background(), spotify.ID(playlistId))
		if err != nil {
			fmt.Println(err)
			return
		}
		s.playlists[s.LoadUserPlaylists()] = nil
		p := s.playlist[playlistId]
		s.playlist[playlistId] = NewPlaylist(playlistId, p.Name, p.Owner, p.Image, uint(p.Followers), uint(p.TotalSize), p.tracks, p.Kind, playlistFollowFalse)
		s.LoadUserPlaylists()
	} else if kind == playlistKindAlbum {
		err := s.spotAPIClient.RemoveAlbumsFromLibrary(context.Background(), spotify.ID(playlistId))
		if err != nil {
			fmt.Println(err)
			return
		}
		s.playlists[s.LoadUserAlbums()] = nil
		p := s.albums[playlistId]
		s.albums[playlistId] = NewAlbum(playlistId, p.Name, p.Group, []spotify.SimpleArtist{}, p.Image, p.tracks, playlistFollowTrue)
		s.LoadUserAlbums()
	} else if kind == playlistKindShow {
		fmt.Println("Not implemented :'(")
	}
}
