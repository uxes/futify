package server

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"time"

	"github.com/librespot-org/librespot-golang/librespot"
	"github.com/nanu-c/qml-go"
	"github.com/zmb3/spotify/v2"
	spotifyauth "github.com/zmb3/spotify/v2/auth"
	"golang.org/x/oauth2"
)

const (
	cacheSession = "session"

	tokenFutify = "ddf3c08dc7bc4d90b8f8f35d42f9b992"
)

var (
	cachePath  = "/home/phablet/.cache/futify.frenchutouch/"
	configPath = "/home/phablet/.config/futify.frenchutouch/"
)

func init() {
	tmpCachePath := os.Getenv("XDG_CACHE_HOME")
	if tmpCachePath != "" {
		cachePath = path.Join(tmpCachePath, "futify.frenchutouch")
		fmt.Println(fmt.Sprintf("Env $XDG_CACHE_HOME set use it = %s", cachePath))
	} else {
		fmt.Println(fmt.Sprintf("Env $XDG_CACHE_HOME no set use default = %s", cachePath))
	}

	tmpConfigPath := os.Getenv("$XDG_CONFIG_HOME")
	if tmpConfigPath != "" {
		configPath = path.Join(tmpConfigPath, "futify.frenchutouch")
		fmt.Println(fmt.Sprintf("Env $XDG_CONFIG_HOME set use it = %s", configPath))
	} else {
		fmt.Println(fmt.Sprintf("Env $XDG_CONFIG_HOME no set use default = %s", configPath))
	}
}

func (s *Session) TryLogin() bool {
	resp := make(chan bool, 1)
	defer close(resp)
	go func() {
		if blob, err := getFromCache(cacheSession, "current"); err == nil {
			fmt.Println("before restore session")
			parts := bytes.Split(blob, []byte("\n"))
			if len(parts) < 2 {
				resp <- false
				return
			}
			session, err := librespot.LoginSaved(string(parts[0]), parts[1], "Futify")
			fmt.Println("after restore session")
			if err == nil {
				//we are logged
				s.IsLogged = true
				s.spotSession = session
				s.Username = session.Username()

				accessToken, err := s.spotSession.Mercury().GetToken(tokenFutify, "playlist-read-collaborative,playlist-read-private,playlist-modify-private,playlist-modify-public,user-read-recently-played,user-top-read,user-library-read,user-library-modify")
				if err != nil {
					fmt.Println("Can't retreive token " + err.Error())
				}
				d, _ := time.ParseDuration(fmt.Sprintf("%ds", accessToken.ExpiresIn))
				t := oauth2.Token{AccessToken: accessToken.AccessToken, Expiry: time.Now().Add(d), TokenType: accessToken.TokenType}

				httpClient := spotifyauth.New().Client(context.Background(), &t)
				s.spotAPIClient = spotify.New(httpClient)

				resp <- true
				return
			}
		}
		resp <- false
	}()
	return <-resp
}

func (s *Session) Login(username, password string) string {
	session, err := librespot.Login(username, password, "Futify")
	if err != nil {
		fmt.Println("Error during login = " + err.Error())
		return err.Error()
	}
	fmt.Println(("keep login"))
	forCache := append([]byte(fmt.Sprintf("%s\n", username)), session.ReusableAuthBlob()...)
	_, err = writeInCache(bytes.NewReader(forCache), cacheSession, "current")
	if err != nil {
		fmt.Println("Error during save login = " + err.Error())
		return "Can't save session"
	}
	s.spotSession = session
	s.IsLogged = true
	qml.Changed(s, &s.IsLogged)
	s.Username = session.Username()
	qml.Changed(s, &s.Username)
	fmt.Println("User logged with " + s.Username)
	//TODO make it a function and avoid duplicate code with tryLogin
	accessToken, err := s.spotSession.Mercury().GetToken(tokenFutify, "playlist-read-collaborative,playlist-read-private,playlist-modify-private,playlist-modify-public,user-read-recently-played,user-top-read,user-library-read,user-library-modify")
	if err != nil {
		fmt.Println("Can't retreive token " + err.Error())
	}
	d, _ := time.ParseDuration(fmt.Sprintf("%ds", accessToken.ExpiresIn))
	t := oauth2.Token{AccessToken: accessToken.AccessToken, Expiry: time.Now().Add(d), TokenType: accessToken.TokenType}

	httpClient := spotifyauth.New().Client(context.Background(), &t)
	s.spotAPIClient = spotify.New(httpClient)
	return ""
}

func (s *Session) LoadAlbum(albumUuid string) string {
	if s.GetAlbum(albumUuid) != nil {
		// already loaded don't redwnload it
		fmt.Println("Found uuid return " + albumUuid)
		return albumUuid
	}
	a, err := s.spotAPIClient.GetAlbum(context.Background(), spotify.ID(albumUuid))
	if err != nil {
		fmt.Println(err)
	}

	myTracks := make([]*Track, len(a.Tracks.Tracks))
	for index, t := range a.Tracks.Tracks {
		myTracks[index] = NewTrackSimple(t, a, s)
	}
	isFollow := playlistFollowFalse
	for _, p := range s.playlists[s.LoadUserAlbums()].playlists {
		if p.Uuid == albumUuid {
			isFollow = playlistFollowTrue
		}
	}
	s.albums[albumUuid] = NewAlbum(albumUuid, a.Name, a.AlbumGroup, a.Artists, SelectImageURL(a.Images), myTracks, isFollow)
	fmt.Println(fmt.Sprintf("2 Found %d songs added in %s", len(myTracks), albumUuid))
	return albumUuid
}

func (s *Session) EndQueue(t *Track) {
	s.albums["queue"].tracks = append(s.albums["queue"].tracks, t)
	s.albums["queue"].Size = len(s.albums["queue"].tracks)
}

func (s *Session) DeleteQueue(index int) {
	s.albums["queue"].tracks = append(s.albums["queue"].tracks[:index], s.albums["queue"].tracks[index+1:]...)
	s.albums["queue"].Size = len(s.albums["queue"].tracks)
}

func (s *Session) ClearQueue() {
	s.albums["queue"].tracks = make([]*Track, 0)
	s.albums["queue"].Size = 0
}

func (s *Session) LoadShow(showUuid string) string {
	if s.GetShow(showUuid) != nil {
		// already loaded don't redwnload it
		fmt.Println("Found uuid return " + showUuid)
		return showUuid
	}
	a, err := s.spotAPIClient.GetShow(context.Background(), spotify.ID(showUuid))
	if err != nil {
		fmt.Println(err)
	}
	myTracks := make([]*Track, len(a.Episodes.Episodes))
	for index, t := range a.Episodes.Episodes {
		myTracks[index] = NewEpisode(&t, s)
	}
	isFollow := playlistFollowFalse
	for _, p := range s.playlists[s.LoadUserShows()].playlists {
		if p.Uuid == showUuid {
			isFollow = playlistFollowTrue
		}
	}
	s.show[showUuid] = NewShow(showUuid, a.Name, a.Publisher, SelectImageURL(a.Images), 0, myTracks, isFollow)
	fmt.Println(fmt.Sprintf("3 Found %d songs added in %s", len(myTracks), showUuid))
	return showUuid
}

func (s *Session) SaveSettings(quality int, theme int, lang int, errorSong int) {
	fmt.Println(fmt.Sprintf("Receive %d", lang))
	if lang != s.Settings.Lang {
		defer s.LoadLang()
	}
	s.Settings.Quality = quality
	s.Settings.Theme = theme
	s.Settings.Lang = lang
	s.Settings.ErrorSong = errorSong
	s.writeSettings()
}

func (s *Session) LoadLang() {
	fmt.Println(fmt.Sprintf("Load %s", s.locale()))
	if s.Settings.Lang > 0 {
		err := qml.LoadTranslator(s.locale(), "qml", "_", "i18n", ".qm")
		if err != nil {
			fmt.Println(fmt.Sprintf("Can't load translation %d - %s", s.Settings.Lang, s.locale()))
		}
	} else {
		err := qml.LoadTranslatorCurrentLocale("qml", "_", "i18n", ".qm")
		if err != nil {
			fmt.Println(fmt.Sprintf("Can't load default translation %d - %s", s.Settings.Lang, s.locale()))
		}
	}
	s.engine.Retranslate()
}

func (s *Session) Logout() {
	s.IsLogged = false
	s.Username = ""
	s.Settings = &Settings{Quality: 1, Theme: 0, Lang: 0, ErrorSong: 0}
	s.albums = make(map[string]*Album)
	s.playlists = make(map[string]*Playlists)
	s.playlist = make(map[string]*Playlist)
	s.tracks = make(map[string]*Track)
	os.Remove(path.Join(cachePath, cacheSession, "current"))
}

func writeInCache(r io.Reader, typeBin string, filename string) (string, error) {
	dir := path.Join(cachePath, typeBin)
	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		return "", err
	}
	path := path.Join(dir, filename)
	fo, err := os.Create(path)
	if err != nil {
		return "", err
	}

	// close fo on exit and check for its returned error
	defer func() {
		if err := fo.Close(); err != nil {
			fmt.Println("Error during close of file cache", err)
		}
	}()
	// make a write buffer
	w := bufio.NewWriter(fo)

	// make a buffer to keep chunks that are read
	buf := make([]byte, 1024)
	for {
		// read a chunk
		n, err := r.Read(buf)
		if err != nil && err != io.EOF {
			fmt.Println("Can't read buffer for write file cache", err)
			return "", err
		}
		if n == 0 {
			break
		}

		// write a chunk
		if _, err := w.Write(buf[:n]); err != nil {
			fmt.Println("Can't write buffer for write file cache", err)
			return "", err
		}
	}

	if err = w.Flush(); err != nil {
		fmt.Println("Can't flush buffer for write file cache", err)
		return "", err
	}

	fmt.Println(fmt.Sprintf("Finishing writing %s", path))

	return path, nil
}

func getFromCache(typeBin string, filename string) ([]byte, error) {
	return ioutil.ReadFile(path.Join(cachePath, typeBin, filename))
}

func (s *Session) writeSettings() error {
	data, err := json.Marshal(s.Settings)
	if err != nil {
		fmt.Println("Can't marshall settings")
	}
	if err := os.MkdirAll(configPath, os.ModePerm); err != nil {
		fmt.Println(fmt.Sprintf("Can't mkdirAll %s", configPath))
		return err
	}
	if err := ioutil.WriteFile(path.Join(configPath, "settings.json"), data, os.ModePerm); err != nil {
		fmt.Println("can't write settings")
		return err
	}
	return nil
}

func (s *Session) NbContributors() int {
	return len(s.contributors)
}

func (s *Session) GetContributor(index int) *Contributors {
	return s.contributors[index]
}
