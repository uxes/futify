<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu_HU">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/pages/About.qml" line="9"/>
        <source>About</source>
        <translation>Bővebben</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="29"/>
        <source>App:</source>
        <translation>Alkalmazás:</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="36"/>
        <source>Futify is an unofficial native spotify client.</source>
        <translation>A Futify egy nem hivatalos natív Spotify kliens.</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="48"/>
        <source>issues</source>
        <translation>problémák</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="61"/>
        <source>Author:</source>
        <translation>Fejlesztő:</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="94"/>
        <source>Contributors:</source>
        <translation>Közreműködők:</translation>
    </message>
</context>
<context>
    <name>AlbumView</name>
    <message>
        <location filename="../qml/components/AlbumView.qml" line="90"/>
        <source>Remove from my library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/AlbumView.qml" line="90"/>
        <source>Add to my library</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Albums</name>
    <message>
        <location filename="../qml/components/Albums.qml" line="72"/>
        <source>Play</source>
        <translation>Lejátszás</translation>
    </message>
</context>
<context>
    <name>Artists</name>
    <message>
        <location filename="../qml/components/Artists.qml" line="64"/>
        <source>popularity</source>
        <translation>népszerűség</translation>
    </message>
</context>
<context>
    <name>Home</name>
    <message>
        <location filename="../qml/pages/Home.qml" line="85"/>
        <source>Search</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="100"/>
        <source>Settings</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="105"/>
        <source>About</source>
        <translation>Bővebben</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="134"/>
        <source>Your playlists</source>
        <translation>Saját lejátszási listák</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="164"/>
        <source>Your albums</source>
        <translation>Saját albumok</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="194"/>
        <source>Your shows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="224"/>
        <source>Your tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Recently Play</source>
        <translation type="vanished">Mostanában Hallgatott</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="254"/>
        <source>Featured playlist</source>
        <translation>Ajánlott lejátszási listák</translation>
    </message>
    <message>
        <source>Top tracks</source>
        <translation type="vanished">Kedvenc számok</translation>
    </message>
</context>
<context>
    <name>HorizontalList</name>
    <message>
        <location filename="../qml/components/HorizontalList.qml" line="44"/>
        <source>Show more...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../qml/pages/Login.qml" line="34"/>
        <source>Username</source>
        <translation>Felhasználónév</translation>
    </message>
    <message>
        <location filename="../qml/pages/Login.qml" line="40"/>
        <source>Password</source>
        <translation>Jelszó</translation>
    </message>
    <message>
        <location filename="../qml/pages/Login.qml" line="53"/>
        <source>Log in</source>
        <translation>Belépés</translation>
    </message>
    <message>
        <location filename="../qml/pages/Login.qml" line="67"/>
        <source>spotify.com</source>
        <translation>spotify.com</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../qml/Main.qml" line="190"/>
        <source>Followed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="197"/>
        <source>Unfollowed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="218"/>
        <location filename="../qml/Main.qml" line="246"/>
        <source>Added</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="225"/>
        <location filename="../qml/Main.qml" line="253"/>
        <source>Removed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="383"/>
        <source>Shuffle activated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="386"/>
        <source>Repeat activated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="389"/>
        <source>Repeat one activated</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlayerView</name>
    <message>
        <location filename="../qml/components/PlayerView.qml" line="82"/>
        <source>No song</source>
        <translation>Nincs szám</translation>
    </message>
    <message>
        <location filename="../qml/components/PlayerView.qml" line="82"/>
        <source>Buffering</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlaylistView</name>
    <message>
        <location filename="../qml/components/PlaylistView.qml" line="85"/>
        <source>followers</source>
        <translation>követő</translation>
    </message>
    <message>
        <location filename="../qml/components/PlaylistView.qml" line="86"/>
        <source>tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PlaylistView.qml" line="90"/>
        <source>Unfollow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PlaylistView.qml" line="90"/>
        <source>Follow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Playlists</name>
    <message>
        <source>Play</source>
        <translation type="vanished">Lejátszás</translation>
    </message>
</context>
<context>
    <name>PlaylistsType</name>
    <message>
        <location filename="../qml/model/PlaylistsType.qml" line="15"/>
        <source>Your playlists</source>
        <translation type="unfinished">Saját lejátszási listák</translation>
    </message>
    <message>
        <location filename="../qml/model/PlaylistsType.qml" line="17"/>
        <location filename="../qml/model/PlaylistsType.qml" line="23"/>
        <source>Your albums</source>
        <translation type="unfinished">Saját albumok</translation>
    </message>
    <message>
        <location filename="../qml/model/PlaylistsType.qml" line="19"/>
        <source>Featured playlist</source>
        <translation type="unfinished">Ajánlott lejátszási listák</translation>
    </message>
    <message>
        <location filename="../qml/model/PlaylistsType.qml" line="21"/>
        <source>Your tracks</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlaylistsView</name>
    <message>
        <location filename="../qml/components/PlaylistsView.qml" line="72"/>
        <source>Play</source>
        <translation type="unfinished">Lejátszás</translation>
    </message>
</context>
<context>
    <name>Queue</name>
    <message>
        <location filename="../qml/pages/Queue.qml" line="31"/>
        <source>Queue</source>
        <translation>Sor</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="35"/>
        <source>Next</source>
        <translation>Következő</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="43"/>
        <source>Play/Pause</source>
        <translation>Lejátszás/Megállítás</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="55"/>
        <source>Precedent</source>
        <translation>Előző</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="63"/>
        <source>Clear</source>
        <translation>Töröl</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="72"/>
        <source>Repeat</source>
        <translation>Ismétlés</translation>
    </message>
</context>
<context>
    <name>Search</name>
    <message>
        <location filename="../qml/pages/Search.qml" line="42"/>
        <source>Search</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="58"/>
        <source>Tracks</source>
        <translation>Számok</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="69"/>
        <source>Episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="80"/>
        <source>Albums</source>
        <translation>Albumok</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="91"/>
        <source>Playlists</source>
        <translation>Lejátszási listák</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="102"/>
        <source>Shows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="113"/>
        <source>Artists</source>
        <translation>Előadók</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="27"/>
        <source>Settings</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="34"/>
        <source>Reset</source>
        <translation>Visszaállít</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="43"/>
        <source>Save</source>
        <translation>Mentés</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="88"/>
        <source>Theme:</source>
        <translation>Téma:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="97"/>
        <source>Dark</source>
        <translation>Sötét</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="97"/>
        <source>Light</source>
        <translation>Világos</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="97"/>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>System</source>
        <translation>Rendszer</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="145"/>
        <source>Lang:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>German</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>Spanish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>French</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>Hungarian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>Dutch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="202"/>
        <source>Quality:</source>
        <translation>Minőség:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="211"/>
        <source>low: prefer data over quality</source>
        <translation>alacsony: adatforgalom előnyben a minőséggel szemben</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="211"/>
        <source>medium</source>
        <translation>közepes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="211"/>
        <source>high: prefer quality over data</source>
        <translation>magas: minőség előnyben az adatforgalommal szemben</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="259"/>
        <source>Error song:</source>
        <translation>Hiba zene:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="268"/>
        <source>silence: 3 seconds</source>
        <translation>csend: 3 másodperc</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="268"/>
        <source>error: usefull to know what happens</source>
        <translation>hiba: hasznos tudni, mi történt</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="315"/>
        <source>Account:</source>
        <translation>Fiók:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="326"/>
        <source>Log out</source>
        <translation>Kilépés</translation>
    </message>
</context>
<context>
    <name>ShowView</name>
    <message>
        <location filename="../qml/components/ShowView.qml" line="92"/>
        <source>Unfollow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ShowView.qml" line="92"/>
        <source>Follow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Shows</name>
    <message>
        <location filename="../qml/components/Shows.qml" line="72"/>
        <source>Play</source>
        <translation type="unfinished">Lejátszás</translation>
    </message>
</context>
<context>
    <name>TrackListItem</name>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="55"/>
        <source>popularity</source>
        <translation>népszerűség</translation>
    </message>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="63"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="74"/>
        <source>Play</source>
        <translation>Lejátszás</translation>
    </message>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="82"/>
        <source>End Queue</source>
        <translation>Sor Vége</translation>
    </message>
</context>
<context>
    <name>UserMetricsHelper</name>
    <message>
        <location filename="../qml/services/UserMetricsHelper.qml" line="15"/>
        <source>tracks played today</source>
        <translation>ma játszott szám</translation>
    </message>
    <message>
        <location filename="../qml/services/UserMetricsHelper.qml" line="16"/>
        <source>No tracks played today</source>
        <translation>Ma nem játszottál le számokat</translation>
    </message>
</context>
</TS>
